var express = require('express');
var router = express.Router();
var io = require('socket.io');

/* GET home page. */
router.get('/scoreboard', function(req, res, next) {
  res.render('scoreboard', { title: 'Scoreboard' });
});

router.get('/admin', function(req, res, next) {
  res.render('admin', { title: 'Manage Scoreboard' });
});

module.exports = router;

