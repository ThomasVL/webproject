//Declarations
var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var concatCSS = require('gulp-concat-css');

//Tasks
gulp.task('sass', function(){
  return gulp.src('./public/sass/style.scss')
    .pipe(sass())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./public/stylesheets'))
});

/*
gulp.task('concatcss', function() {
    return gulp.src('./public/sass/style.scss')
        .pipe(concatCSS('./public/build/style.css'))
        .pipe(gulp.dest('./public/build/style.css'));
});
*/


//Watch
gulp.task('watch',function() {
    gulp.watch('./public/sass/**/*.scss', ['sass']);
    //gulp.watch('./public/sass/**/*.scss', ['concatcss']);
});

gulp.task('default', ['sass']);